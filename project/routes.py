from flask import Blueprint, render_template, redirect, url_for, flash
from .forms import RegistrationForm, LoginForm
from .models import Users, db
from flask import Blueprint, jsonify, request
from flask_login import login_user, login_required, current_user, logout_user

auth = Blueprint('auth', __name__)


@auth.route('/alive', methods=['GET'])
def alive():
    return "Alive"

@auth.route('/')
@auth.route('/signup', methods=['GET', 'POST'])
def signup():
    form = RegistrationForm()
    if form.validate_on_submit():
        new_user = Users(username=form.username.data, email=form.email.data, password=form.password.data)
        print(new_user)
        db.session.add(new_user)
        db.session.commit()
        print("success")
        # Save new_user to the database
        flash('Account created successfully. You can now log in.', 'success')
        return redirect(url_for('auth.login'))
    return render_template('signup.html', form=form)

@auth.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        # Check if the user exists and the password is correct
        user = Users.query.filter_by(email=form.email.data).first()
        if user and user.password == form.password.data:
            login_user(user)
            flash('Logged in successfully.', 'success')
            return redirect(url_for('auth.profile'))
        else:
            flash('Invalid email or password.', 'danger')
    return render_template('login.html', form=form)
       

@auth.route('/profile')
@login_required
def profile():
    # Get the user's profile information
    return render_template('profile.html', user=current_user)

@auth.route('/users', methods=['GET'])
@login_required
def get_users():
    users = Users.query.all()
    user_list = [{'id': user.id, 'username': user.username, 'email': user.email} for user in users]
    return render_template('users.html', user_list=user_list)

@auth.route('/logout')
def logout():
    logout_user()
    flash(f'You are currently logged out!', category='info')
    return redirect(url_for('auth.signup'))

