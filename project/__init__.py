from flask import Flask
from project.models import db, migrate, login_manager
from project.routes import auth


def create_app():
    app = Flask(__name__)
    app.config.from_object('config.BaseConfig')
    db.init_app(app)
    migrate.init_app(app, db)
    login_manager.init_app(app)
    app.register_blueprint(auth)
    return app 