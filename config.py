import os

class BaseConfig:
    SECRET_KEY = 'your_secret_key'
    POSTGRES_HOST = os.environ.get('POSTGRES_HOST', '36.255.70.130')
    POSTGRES_PORT = os.environ.get('POSTGRES_PORT', 5432)
    POSTGRES_USER = os.environ.get('POSTGRES_USER','postgres')
    POSTGRES_PASSWORD = os.environ.get('POSTGRES_PASSWORD','rauf')
    # print(POSTGRES_PASSWORD, POSTGRES_PASSWORD)
    POSTGRES_DB = os.environ.get('POSTGRES_DB', 'usr_db')

    SQLALCHEMY_DATABASE_URI = f'postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_HOST}:{POSTGRES_PORT}/{POSTGRES_DB}'
    print(SQLALCHEMY_DATABASE_URI)