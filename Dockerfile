# Base Image
FROM python:3.10-slim-buster

# set working directory
WORKDIR /usr/app


# copy and install necessary dependencies
COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .
EXPOSE 5000
# boot configuration for container
# CMD ["flask", "run", "--host=0.0.0.0"]
CMD gunicorn manage:app -w 4 --log-level debug --bind 0.0.0.0:5000

